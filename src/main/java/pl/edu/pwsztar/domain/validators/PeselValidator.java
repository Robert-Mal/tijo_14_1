package pl.edu.pwsztar.domain.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PeselValidator {

  public static boolean isValid(final String pesel) {

    String regex = "^[0-9]{11}$";
    Pattern pattern = Pattern.compile(regex);
    Matcher matcher = pattern.matcher(pesel);

    if (!matcher.matches()) {
      return false;
    }

    return isChecksumValid(pesel);
  }

  private static boolean isChecksumValid(final String pesel) {
    String[] peselSplitted = pesel.split("");
    int[] weights = {1, 3, 7, 9, 1, 3, 7, 9, 1, 3};

    int S = 0;
    for (int i = 0; i < 10; ++i) {
      S += Integer.parseInt(peselSplitted[i]) * weights[i];
    }

    int M = S % 10;

    if (M == 0 && Integer.parseInt(peselSplitted[peselSplitted.length - 1]) == 0) {
      return true;
    }

    return (10 - M) == Integer.parseInt(peselSplitted[peselSplitted.length - 1]);
  }
}
